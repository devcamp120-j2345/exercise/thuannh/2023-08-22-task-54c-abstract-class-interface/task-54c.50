package com.devcamp.interfaces;

public interface Movable {
    public void moveUp();

    public void moveDown();

    public void moveLeft();

    public void moveRight();
}
