import com.devcamp.models.MovableCircle;
import com.devcamp.models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point = new MovablePoint(10, 15, 4, 7);

        System.out.println(point.toString());

        point.moveUp();
        point.moveUp();
        point.moveLeft();
        System.out.println(point.toString());

        MovableCircle circle = new MovableCircle(5, point);

        System.out.println(circle.toString());

        circle.moveRight();
        circle.moveDown();
        System.out.println(circle.toString());

    }
}
